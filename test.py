def find_entry_in_txt(target_string):
    with open( "xmlData/fileNames.txt", 'r') as file:
        for line in file:
            if target_string in line:
                return line.strip()  # Removing any trailing whitespaces or newlines
    return None  # Return None if the string is not found

# Example usage:
result = find_entry_in_txt("72650")
if result:
    print(f"Entry found: {result}")
else:
    print("Entry not found.")
