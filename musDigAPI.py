import requests

# https://www.landesmuseum-stuttgart.de/lmwallobjects.json
## Swagger
# https://lmw.museum-digital.de/swagger/

apiUrl = "https://lmw.museum-digital.de/json/objects"
headers = {"Accept":"application/json"} 

def getObjects(searchParams):
    searchParams["gbreitenat"] = 100
    searchParams["startwert"] = 0
    searchParams["navlang"] = "de"
    r = requests.get(f"{apiUrl}", params=searchParams, headers=headers)
    return (r.json())

if __name__ == '__main__':
    inventarnr = "KK weiß 5" # Liefert Ergebnis
    #inventarnr = "R FU.131" # Liefert kein Ergebnis (status: Error: msg: There are no results for this query.)
    searchParams = {}
    searchParams["s"] = f"invo:'{inventarnr}'"
    objects = getObjects(searchParams)
    try:
        for object in objects:
            if (object["objekt_inventarnr"]) == inventarnr:
                print(object)
    except TypeError:
        #print(objects["status"]+": "+objects["msg"])
        pass