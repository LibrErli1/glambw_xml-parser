# GLAM-BW XML Parser

Dieses Verzeichnis beinhaltet Python Skripte die dazu dienen, XML-Exporte von Metadaten in zwei unterschiedlichen Formaten (imdas-pro oder lido) zu serialisieren und in ein `json` zu überführen um damit übersichtliche OpenRefine-Projekte starten zu können.

## Verwendung

Es stehen zwei unterschiedliche Parser zur Verfügung.

* `main.py` wurde ursprünglich für das `imdas pro` Format entwickelt
* `runLidoXML.py` ist für XML-Datensätze im `lido` Format entwickelt.

### `main.py` für `imdas` Datensätze

* Einen imdas Export als `*.xml` in das Verzeichnis `xmlData` speichern.
* In der `config.json` Datei den Eintrag `"inputFileName" : "xmlData/Export_20230929_9DS.xml",` entsprechend an den Dateinamen des Exportes anpassen.
* Die Pythondatei ausführen `python3 main.py`
* Es wird eine `output.xlsx` und eine `output.json` Datei erzeugt, die für den Import in OpenRefine verwendet werden kann..

### `runLidoXML.py` für `lido:xml` Datensätze

* In das Verzeichnis `xmlLIdoData` können einzelne Lido-XML-Datensätze hochgeladen werden.
* In der `configLido.json` Datei den Eintrag `"inputFilePath" : "xmlLidoData",` entsprechend an den Namen des Verzeichnisses mit den xmlLido-Dateien anpassen, wenn nötig.
* Die Pythondatei ausführen `python3 runLidoXML.py`
* Es wird eine `output_Lido.csv` und eine `output_Lido.json` Datei erzeugt, die für den Import in OpenRefine verwendet werden kann.

## Ausführung in PAWS

* Anmelden mit MediaWiki-User in `https://hub-paws.wmcloud.org/`
* Öffnen eines Terminal-Fensters: File -> New -> Terminal
* Wechseln in das Skriptverzeichnis `cd glambw_xml-parser`
* Ausführen des Skripts für `imdas pro`-Datensätze: `python3 main.py`
* Ausführen des Skripts für `lido:xml`-Datensätze: `python3 runLidoXML.py`
* Das Verzeichnis `glambw_xml-parser` ist synchron mit dem GitLab-Repository: https://gitlab.com/LibrErli1/glambw_xml-parser
  * Änderungen am Skript können aus dem Repository mit `git pull` in das Verzeichnis geladen werden.

## Konfiguration

Für die beiden Skripte stehen jeweils eigene `config*.json` Dateien zur Verfügung. 

* In `config.json` wird bspw. der `InputFileName` also der Dateiname zum xml-File des imdas-Exportes festgelegt.
* In `configLido.json` wird das Verzeichnis mit den lido-XML-Dateien in `InputFilePath` definiert.

Der Hauptteil der beiden Konfigurationen stellt aber das Mapping des Ausgangsformates in das serialisierte und normalisierte `output*.json` dar.

Dieses Mapping besteht grundsätzlich aus folgenden Werten:
* `xmlNode` : Hier wird der Name des Feldes (imdas) bzw. ein Xpath (lido) zum jeweiligen Ausgangsmetadataum eingetragen.
* `result` Das ist der Name des Ergebnisfeldes in der `output*`-Datei
* `"isMultiple": true` kann angegeben werden, wenn das Feld in der Ausgangsdatei wiederholbar ist. 

Die Imdas-Verarbeitung kennt aufgrund der stark verschachtelten XML-Struktur noch weitere Paramter wie `childNodes` um spezifische Kindlemente abzufragen oder `transponeNode` um zeilenhafte Werte in die Spaltestruktur zu "transponieren". (D.h. bei transponeNode werden die Kinderelemente des angegebenen Ausgangsmetadatums derart transponiert, dass aus den Tag-Names die Spalten der Output-Tabelle verwenden, mit den entsprechenden Inhalten )