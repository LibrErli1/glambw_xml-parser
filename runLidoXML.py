import readXML as readXML
import musDigAPI as musDig
import json
import pandas as pd
import os
# read and parse ConfigFile
with open('configLido.json', 'r', encoding='utf-8') as configFile:
    data=configFile.read()
config = json.loads(data)
#print(config)

# Define the directory containing the XML files
directory_path = config["inputFilePath"]

# List all files in the directory
all_files = os.listdir(directory_path)

resultSet = []
for file in all_files:

    inputFileName = os.path.join(directory_path, file)
    root = readXML.parseMetadataFile(inputFileName)
    #print(root)


    resultDet = {}
    for metadataMap in config["mapping"]:
        #print(map["xmlNode"])
        element = readXML.findMetadata(root,xpathStr=metadataMap["xmlNode"])
        try:
            if metadataMap["isMultiple"] == True:
                innerResultSet = []
                for metadata in element:
                    print(f"{metadataMap['result']}: {metadata.text}")
                    innerResultSet.append(metadata.text)
                resultDet[metadataMap['result']] = innerResultSet
        except KeyError:
            print(f"{metadataMap['result']}: {element[0].text}")
            resultDet[metadataMap['result']] = element[0].text

    resultSet.append(resultDet)
print(resultSet)

#Schreibe Metadaten (resultSet) in ein XLS-File zur Weiterbearbeitung
df = pd.DataFrame(resultSet) 
print(df)
#df.to_excel('Output_Lido.xlsx') 
df.to_csv('Output_Lido.csv')

# Schreibe Metadaten (resultSet) in ein JSON-File
json_str = json.dumps(resultSet)
# And then write that string to a file
with open('Output_Lido.json', 'w') as f:
    f.write(json_str)