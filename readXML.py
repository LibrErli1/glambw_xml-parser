import lxml.etree as ET
from urllib.parse import urlparse
import os

def parseMetadataFile(filename):
    #mytree = ET.parse(f'metadata/{processId}/meta.xml',parser=ET.XMLParser(remove_blank_text=True))
    mytree = ET.parse(filename, parser=ET.XMLParser(remove_blank_text=True))
    mytree = mytree.getroot()
    return mytree

def getImageFileName(imageUrl):
    a = urlparse(imageUrl)
    return  (os.path.splitext(os.path.basename(a.path))[0]) 

def findMasterImage(target_string):
    with open( "xmlData/fileNames.txt", 'r') as file:
        for line in file:
            if target_string in line:
                return line.strip()  # Removing any trailing whitespaces or newlines
    return None  # Return None if the string is not found

def findMetadata(mytree, attributeName="", xpathStr = ""):
    namespaces = {'lido' : 'http://www.lido-schema.org'}
    ET.register_namespace=namespaces
    e = mytree.xpath(xpathStr, namespaces=namespaces)
    #print(e)
    return e