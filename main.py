import readXML as readXML
import musDigAPI as musDig
import json
import pandas as pd

# read and parse ConfigFile
with open('config.json', 'r', encoding='utf-8') as configFile:
    data=configFile.read()
config = json.loads(data)

#inputFileName (i.e. *.xml for object metadata)
inputFileName = config["inputFileName"]

root = readXML.parseMetadataFile(inputFileName)
#print(root)

xpathFindStr = './/OBJECT'
objects = root.findall(xpathFindStr)
#print(objects)

resultSet = []
for obj in objects:
    resultDet = {}
    for parseMapper in config["mapping"]:

        try:
            resultKeyElement = obj.xpath(parseMapper["resultNode"])
            try:
                resultKey = (resultKeyElement[0].text) + parseMapper["resultNodeSuffix"]
            except KeyError:
                resultKey = (resultKeyElement[0].text)
            xPathStr = f"{parseMapper['xmlNode']}[{parseMapper['xmlMatchNode']}='{resultKeyElement[0].text}']/{parseMapper['innerXmlNode']}"
            nodeElement = obj.xpath(xPathStr)

        except KeyError:
            try:
                dict_list = []
                nodeElements = obj.findall(parseMapper["transponeNode"])
                # Loop through child elements of OBJ_VALUE
                for node in nodeElements:
                    value_dict = {}
                    for child in node:
                        if parseMapper["transponeNode"] == "OBJ_VALUE":
                            # Use OBJEKTTEIL as key if present, else use UNIT_NAME
                            key_name = node.find('OBJEKTTEIL').text if node.find('OBJEKTTEIL') is not None else node.find('UNIT_NAME').text
                            value = node.find('VALUE').text
                            unit = node.find('UNIT').text

                            # Add the key-value pairs to the dictionary
                            resultDet[key_name] = value
                            resultDet[key_name + '_UNIT'] = unit
                        elif parseMapper["transponeNode"] == "PERSON":
                            key_name = node.find("ROLLE_FUNKTION").text
                            resultDet[key_name] = node.find('ANZEIGENAME').text 
                            print(resultDet)
                        else:
                            # Add child tag as key and child text as value
                            value_dict[child.tag] = child.text
                        #print(value_dict)
                    dict_list.append(value_dict)
                if parseMapper["transponeNode"] != "OBJ_VALUE" and parseMapper["transponeNode"] != "PERSON":
                    resultDet[parseMapper["transponeResult"]] = dict_list
            except KeyError:
                nodeElement = obj.xpath(parseMapper["xmlNode"])
                resultKey = parseMapper["result"]
                #print(parseMapper["xmlNode"])

        #print(nodeElement)
        if nodeElement:
            try:
                if parseMapper["isMultiple"] == True:
                    innerResult = []
                    for node in nodeElement:
                        if resultKey == "Bild_Url":
                            bildFileName = readXML.getImageFileName(node.text)
                            masterImageFileName = readXML.findMasterImage(bildFileName)
                            innerResult.append(masterImageFileName)
                        else:
                            innerResult.append(node.text)
                    resultDet[resultKey] = innerResult
            except KeyError:
                resultDet[resultKey] = nodeElement[0].text
                #Bei Inventarnummer via Museums-Digital-API die Object-ID für den Link auf die Online-Präsentation abholen
                if parseMapper["xmlNode"] == "INVENTARNUMMER":
                    searchParams = {}
                    searchParams["s"] = f"invo:'{nodeElement[0].text}'"
                    musDigObjects = musDig.getObjects(searchParams)
                    try:
                        for musDigObject in musDigObjects:
                            print(musDigObject)
                            if (musDigObject["objekt_inventarnr"]) == nodeElement[0].text:
                                resultDet["objekt_id"] = (musDigObject["objekt_id"])
                                resultDet["SammlungOnlineURL"] = "https://lmw.museum-digital.de/object/"+str(musDigObject["objekt_id"])
                    except TypeError:
                        pass

    resultSet.append(resultDet)
    #print(resultDet)

#Schreibe Metadaten (resultSet) in ein XLS-File zur Weiterbearbeitung
df = pd.DataFrame(resultSet) 
df.to_excel('Output.xlsx')  

# Schreibe Metadaten (resultSet) in ein JSON-File
json_str = json.dumps(resultSet)
# And then write that string to a file
with open('Output.json', 'w') as f:
    f.write(json_str)